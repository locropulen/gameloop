import React from "react";
const size = 200;
const Box = (props) => {
  const { body } = props
  const { x, y } = React.useMemo(() => {
    return {
      x: body.position.x - size / 2,
      y: body.position.y - size / 2,
    };
  }, [body.position.x, body.position.y]);

  // console.log(props.body)
  return (
    <div
      style={{
        position: "absolute",
        width: size,
        height: size,
        backgroundColor: "red",
        left: x,
        top: y,
      }}
    />
  );
};
export { Box, size };
