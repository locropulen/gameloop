import React from "react";

import Matter from "matter-js";

const width = 800
const height = 60
const ground = Matter.Bodies.rectangle(400, 610, width, height, { isStatic: true });

const Box = (props) => {
  const { body } = props
  const { x, y } = React.useMemo(() => {
    return {
      x: body.position.x - width / 2,
      y: body.position.y - height / 2,
    };
  }, [body.position.x, body.position.y]);

  // console.log(props.body)
  return (
    <div
      style={{
        position: "absolute",
        width,
        height,
        backgroundColor: "red",
        left: x,
        top: y,
      }}
    />
  );
};

const floor = {body: ground, renderer: <Box />}

export { ground, floor }
