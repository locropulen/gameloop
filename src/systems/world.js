import Matter from "matter-js";

import { Box, size } from "../renderer";
import { ground, floor } from "../renderer/floor";

let box = Matter.Bodies.rectangle(200, 200, size, size, {
    // isStatic: true
});
const box1 = {body: box, renderer: <Box />}

let engine = Matter.Engine.create({ enableSleeping: false });

let world = engine.world;

// world.gravity.y = 1;
// world.gravity.scale = 0.002
world.gravity.y = 0.8;
// world.gravity.x = 0.2;
Matter.World.add(world, [ground, box]);

// Matter.Engine.run(engine)

export { engine, world, box1, floor }