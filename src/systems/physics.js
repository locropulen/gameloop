import Matter from "matter-js";

const Physics = (entities, args) => {
//   const world = entities.world;
  let engine = entities.physics.engine;

  Matter.Engine.update(engine, args.time.delta);

  return entities;
};

export default Physics;
