import React from "react";
import { GameEngine } from "react-game-engine";

import "../index.css";
import systems from "../systems";

import { engine, world, box1, floor } from "../systems/world"
const entities = {
    box1,
    floor,
    physics: { engine, world }
}

const SimpleGame = props => {
  return (
    <GameEngine
      systems={systems}
      entities={entities}
    />
  );
}


export default SimpleGame;
